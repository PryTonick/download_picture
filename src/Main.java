import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    private static final String IN_FILE_TXT = "src\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\outFile.txt";
    private static final String PATH_TO_IMG = "picture";

    public static void main(String[] args) {

        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            extractionOfLinks(inFile,outFile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            download(musicFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод извлекает из вебстраниц ссылки на изображения;
     */
    private static void extractionOfLinks(BufferedReader inFile, BufferedWriter outFile) throws IOException {
        String link;
        while ((link = inFile.readLine()) != null) {
            URL url = new URL(link);
            String result;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                result = bufferedReader.lines().collect(Collectors.joining("\n"));
            }
            Pattern email_pattern = Pattern.compile("\\s*(?<=<img [^>]{0,100}src\\s?=\\s?\")[^>\"]+");
            Matcher matcher = email_pattern.matcher(result);
            int i = 0;
            while (matcher.find() && i < 3) {
                outFile.write(matcher.group() + "\r\n");
                i++;
            }
        }
    }

    private static void download(BufferedReader musicFile) {
        String img;
        int count = 0;
        try {
            while ((img = musicFile.readLine()) != null) {
                downloadUsingNIO(img, PATH_TO_IMG + String.valueOf(count) + ".jpg");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод скачивает изображение из интерента с помощью пакета java.nio;
     * @param strUrl ссылка на файл в интернете;
     * @param file путь к файлу, в который будет скачано содержимое из интернета;
     * @throws IOException ошибка ввода-вывода;
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}